function vectorSum(vector) {
    var sum = 0;
    for (var i = 0; i < vector.length; i++) {
        sum += vector[i];
    }
    return sum;

}

function avg(arr) {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
        sum += parseFloat(arr[i]);
    }
    return sum / arr.length;
}

function arrayDiff(arr, avg) {
    var aryDiff = new Array();
    for (var i = 0; i < arr.length; i++) {
        var diff = parseFloat(arr[i]) - avg;
        aryDiff.push(diff);
    }
    return aryDiff;
}

function vectorMultiply(x, y) {
    z = new Array();
    for (var i = 0; i < x.length; i++) {
        z.push(x[i] * y[i]);
    }
    return z;
}

function findy(x, slope, ycept) {
    return (ycept + (slope * x));
}

function randomVector(length, maxvalue) {
    v = new Array();
    for (var i = 0; i < length; i++) {
        v.push(Math.random() * maxvalue);
    }
    return v;

}

function lingres(aryx, aryy) {
    var avgx = avg(aryx);
    console.log("x avg = ", avgx);
    var avgy = avg(aryy);

    var xdiffs = arrayDiff(aryx, avgx);
    var ydiffs = arrayDiff(aryy, avgy);
    //console.log(xdiffs);

    var v_xy = vectorMultiply(xdiffs, ydiffs);
    var v_xdsq = vectorMultiply(xdiffs, xdiffs);
    var v_ydsq = vectorMultiply(ydiffs, ydiffs);

    var sum_xy = vectorSum(v_xy);
    var sum_xdsq = vectorSum(v_xdsq);
    var sum_ydsq = vectorSum(v_ydsq);
    console.log("sums ", sum_xy, sum_xdsq, sum_ydsq);

    //pearson correlation coefficient (r)
    var r = sum_xy / (Math.sqrt(sum_xdsq * sum_ydsq));
    $('#txtPearson').val(r.toString());
    console.log("pearson ", r);

    //standard deviation of x and y
    var stdx = Math.sqrt(sum_xdsq / (aryx.length - 1));
    var stdy = Math.sqrt(sum_ydsq / (aryy.length - 1));
    console.log("stdevs ", stdx, stdy);


    var slope = r * (stdy / stdx);
    console.log("slope ", slope);

    var ycept = avgy - (slope * avgx);
    console.log("ycept ", ycept);

    var line = {
        slope: slope,
        yintercept: ycept,
        vectorX: null,
        vectorY: null
    }
    return line;
}

function drawChart(aryx, aryy, regLine) {
    var scatter = {
        x: aryx,
        y: aryy,
        mode: 'markers'
    };
    var line = {
        x: regLine.vectorX,
        y: regLine.vectorY,
        mode: 'lines'
    };


    var data = [scatter, line];

    var layout = {};

    Plotly.newPlot('divChart', data, layout);
}

function jiggle(v) {
    for (var i = 0; i < v.length; i++) {
        v[i] += Math.random() * 100 * (Math.floor(Math.random() * 2) == 1 ? 1 : -1); 
    }
    return v;
}

function useUserInput() {
    var strx = $('#txtX').val();
    var stry = $('#txtY').val();
    var aryx = strx.split(',');
    var aryy = stry.split(',');
    var line = lingres(aryx, aryy);
    $('#txtEquation').val(`y = ${line.slope}x + ${line.yintercept}`);

    var testx = 15;
    var newy = findy(testx, line.slope, line.yintercept);
    console.log(`for testx ${testx}, newy is ${newy}`);

    var max = aryx.reduce(function (a, b) {
        return Math.max(a, b);
    });
    var min = aryx.reduce(function (a, b) {
        return Math.min(a, b);
    });

    var newVecX = [0, max];
    var newVecY = new Array();
    newVecY[0] = findy(newVecX[0], line.slope, line.yintercept);
    newVecY[1] = findy(newVecX[1], line.slope, line.yintercept);
    line.vectorX = newVecX;
    line.vectorY = newVecY;

    drawChart(aryx, aryy, line);
}

function useRandomValues() {
    var vx = randomVector(1000, 100);
    vx = vx.sort(function (a, b) { return a - b });
    var vy = randomVector(1000, 100);
    vy = vy.sort(function (a, b) { return a - b });
    vy = jiggle(vy);

    //console.log(vx, vy);
    //calculate line object using linear regression
    var line = lingres(vx, vy);
    $('#txtEquation').val(`y = ${line.slope}x + ${line.yintercept}`);
    //test out a new x value
    var testx = 15;
    var newy = findy(testx, line.slope, line.yintercept);
    console.log(`for testx ${testx}, newy is ${newy}`);

    var max = vx.reduce(function (a, b) {
        return Math.max(a, b);
    });
    var min = vx.reduce(function (a, b) {
        return Math.min(a, b);
    });

    var newVecX = [0, max];
    var newVecY = new Array();
    newVecY[0] = findy(newVecX[0], line.slope, line.yintercept);
    newVecY[1] = findy(newVecX[1], line.slope, line.yintercept);
    line.vectorX = newVecX;
    line.vectorY = newVecY;

    drawChart(vx, vy, line);
}

$(document).ready(function () {
    console.log("ready!");
    
});

$('#btnUser').on('click', function (e) { useUserInput(); });
$('#btnRandom').on('click', function (e) { useRandomValues(); });

