# lingres

To learn the math behind linear regression, I've translated the linear regression math equations into Javascript.<br>
Try the live demo here https://albertgt.gitlab.io/lingres/ 


Given vectors x and y, calculate pearson correlation coefficient (r) and standard deviations (s) to calculate slope (m) and y-intercept (a).<br>

![alt text](https://wikimedia.org/api/rest_v1/media/math/render/svg/bd1ccc2979b0fd1c1aec96e386f686ae874f9ec0)

![alt text](https://www.statsdirect.com/help/generatedimages/equations/equation29.svg)

![alt text](https://www.dummies.com/wp-content/uploads/359987.image4.png)

a = y - mx
```javascript

function lingres(aryx, aryy) {
    var avgx = avg(aryx); //mean of vector x
    var avgy = avg(aryy); //mean of vector y

    var xdiffs = arrayDiff(aryx, avgx); // (x - xmean)
    var ydiffs = arrayDiff(aryy, avgy); // (y - ymean)
    
    v_xy = vectorMultiply(xdiffs, ydiffs);
    v_xdsq = vectorMultiply(xdiffs, xdiffs); // (x - xmean)^2
    v_ydsq = vectorMultiply(ydiffs, ydiffs); // (y - ymean)^2

    sum_xy = vectorSum(v_xy);
    sum_xdsq = vectorSum(v_xdsq);
    sum_ydsq = vectorSum(v_ydsq);
    
    //pearson correlation coefficient (r)
    var r = sum_xy / (Math.sqrt(sum_xdsq * sum_ydsq));
    
    //standard deviation of x and y
    var stdx = Math.sqrt(sum_xdsq / (aryx.length - 1));
    var stdy = Math.sqrt(sum_ydsq / (aryy.length - 1));
    
    //slope
    var slope = r * (stdy / stdx);
    
    //y intercept 
    var ycept = avgy - (slope * avgx);
    
    //return a single object containing slope and y intercept
    var line = {
        slope: slope,
        yintercept: ycept
    }
    return line;
}

//
// Helper Functions
//
function vectorSum(vector) {
    var sum = 0;
    for (var i = 0; i < vector.length; i++) {
        sum += vector[i];
    }
    return sum;

}

function avg(arr) {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
        sum += parseFloat(arr[i]);
    }
    return sum / arr.length;
}

function arrayDiff(arr, avg) {
    var aryDiff = new Array();
    for (var i = 0; i < arr.length; i++) {
        var diff = parseFloat(arr[i]) - avg;
        aryDiff.push(diff);
    }
    return aryDiff;
}

function vectorMultiply(x, y) {
    z = new Array();
    for (var i = 0; i < x.length; i++) {
        z.push(x[i] * y[i]);
    }
    return z;
}
```

<h2>Live Demo https://albertgt.gitlab.io/lingres/</h2>

![alt text](https://albertgt.gitlab.io/lingres/lingres1.jpg)

![alt text](https://albertgt.gitlab.io/lingres/lingres2.jpg)
